/*
 * Copyright (C) 2016 Canonical, Ltd.
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 3, as published
 * by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranties of
 * MERCHANTABILITY, SATISFACTORY QUALITY, or FITNESS FOR A PARTICULAR
 * PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef W11TNG_TESTING_BASE_SKELETON_H_
#define W11TNG_TESTING_BASE_SKELETON_H_

#include <ac/glib_wrapper.h>
#include <ac/scoped_gobject.h>

namespace w11tng {
namespace testing {

template <typename T>
class BaseSkeleton {
public:
    BaseSkeleton(T *instance, const std::string &object_path);
    virtual ~BaseSkeleton();

    std::string ObjectPath() const;

protected:
    ac::ScopedGObject<GDBusConnection> bus_connection_;
    ac::ScopedGObject<T> skeleton_;
};

} // namespace testing
} // namespace w11tng

#endif
