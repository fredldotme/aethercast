/*
 * Copyright (C) 2015 Canonical, Ltd.
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 3, as published
 * by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranties of
 * MERCHANTABILITY, SATISFACTORY QUALITY, or FITNESS FOR A PARTICULAR
 * PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <boost/concept_check.hpp>

#include <algorithm>
#include <sstream>

#include <ac/logger.h>
#include <ac/keep_alive.h>
#include <ac/networkutils.h>

#include "w11tng/networkmanager.h"
#include "w11tng/informationelement.h"
#include "w11tng/kernelrfkillmanager.h"
#include "w11tng/urfkillmanager.h"

namespace {
// We take two minutes as timeout here which corresponds to what wpa
// takes for the group formation.
static const std::chrono::seconds kConnectTimeout{120};
// As we play the source role we don't intent to be the group owner
// and therefor use the lowest intent possible.
static constexpr std::int32_t kSourceGoIntent = 0;
}

namespace w11tng {

w11tng::NetworkManagerDevice::Ptr NetworkManagerDevice::Create(NMDevice* device, NMWifiP2PPeer* peer)
{
    return std::shared_ptr<NetworkManagerDevice>(new NetworkManagerDevice(device, peer));
}

NetworkManagerDevice::NetworkManagerDevice(NMDevice* device, NMWifiP2PPeer* peer) :
    ac::NetworkDevice(),
    nmdevice_(device),
    nmpeer_(peer),
    state_(ac::kIdle)
{
}

NetworkManagerDevice::~NetworkManagerDevice()
{
}

ac::MacAddress NetworkManagerDevice::Address() const
{
    auto hw_address = nm_wifi_p2p_peer_get_hw_address(nmpeer_);
    if (!hw_address)
        return ac::MacAddress();
    return ac::MacAddress(hw_address);
}

ac::IpV4Address NetworkManagerDevice::IPv4Address() const
{
    NMIPConfig* ipConfig = nm_device_get_ip4_config(nmdevice_);
    if (!ipConfig)
        return ac::IpV4Address();

    GPtrArray* addresses = nm_ip_config_get_addresses(ipConfig);
    if (!addresses)
        return ac::IpV4Address();

    if (addresses->len <= 0)
        return ac::IpV4Address();

    NMIPAddress* firstAddress = (NMIPAddress*)g_ptr_array_index(addresses, 0);
    return ac::IpV4Address::from_string(nm_ip_address_get_address(firstAddress));
}

std::string NetworkManagerDevice::Name() const
{
    return std::string(nm_wifi_p2p_peer_get_name(nmpeer_));
}

ac::NetworkDeviceState NetworkManagerDevice::State() const
{
    return state_;
}

std::vector<ac::NetworkDeviceRole> NetworkManagerDevice::SupportedRoles() const
{
    std::vector<ac::NetworkDeviceRole> roles;
    roles.push_back(ac::NetworkDeviceRole::kSink);
    return roles;
}

NMDevice* NetworkManagerDevice::NmDevice() const
{
    return nmdevice_;
}

NMWifiP2PPeer* NetworkManagerDevice::NmPeer() const
{
    return nmpeer_;
}

void NetworkManagerDevice::SetState(ac::NetworkDeviceState value)
{
    state_ = value;
}

ac::NetworkManager::Ptr NetworkManager::Create() {
    return std::shared_ptr<NetworkManager>(new NetworkManager())->FinalizeConstruction();
}

std::shared_ptr<NetworkManager> NetworkManager::FinalizeConstruction() {
    auto sp = shared_from_this();

    GError *error = nullptr;
    connection_.reset(g_bus_get_sync(G_BUS_TYPE_SYSTEM, nullptr, &error));
    if (!connection_) {
        AC_ERROR("Failed to connect to system bus: %s", error->message);
        g_error_free(error);
        return sp;
    }

    // We first check if urfkilld is running or not. If its not we fall back
    // to just use the plain rfkill interface the kernel offers.
    AC_DEBUG("Checking if URfkill is available ..");
    urfkill_watch_ = g_bus_watch_name(G_BUS_TYPE_SYSTEM,
                                      URfkillManager::kBusName,
                                      G_BUS_NAME_WATCHER_FLAGS_NONE,
                                      &NetworkManager::OnURfkillAvailable,
                                      &NetworkManager::OnURfkillNotAvailable,
                                      new ac::WeakKeepAlive<NetworkManager>(shared_from_this()),
                                      [](gpointer data) { delete static_cast<ac::WeakKeepAlive<NetworkManager>*>(data); });

    return sp;
}

static void InterfaceDeviceFound(NetworkManager* nm, NMDevice* device, NMClient* client)
{
    if (!nm)
        return;

    nm->OnInterfaceDeviceFound(device);
}

static void InterfaceDeviceLost(NetworkManager* nm, NMDevice* device, NMClient* client)
{
    if (!nm)
        return;

    nm->OnInterfaceDeviceLost(device);
}

NetworkManager::NetworkManager() :
    firmware_loader_("", this),
    dedicated_p2p_interface_(ac::Utils::GetEnvValue("AETHERCAST_DEDICATED_P2P_INTERFACE")),
    session_available_(true),
    urfkill_watch_(0),
    scan_timeout_(0),
    connect_timeout_(0),
    active_connection_(nullptr),
    cancellable(nullptr),
    nm_client(nullptr),
    nm_device_(nullptr)
{
}

NetworkManager::~NetworkManager() {
    Cancel();
}

void NetworkManager::AchieveDeviceAccess(NMClient* client)
{
    if (nm_client != client) {
        nm_client = client;

        g_signal_connect(nm_client,
                         "device-added",
                         (GCallback)InterfaceDeviceFound,
                         (gpointer)this);

        g_signal_connect(nm_client,
                         "device-removed",
                         (GCallback)InterfaceDeviceLost,
                         (gpointer)this);
    }

    const GPtrArray* devices = nm_client_get_all_devices(nm_client);
    for (gint i = 0; i < devices->len; i++) {
        NMDevice* device = (NMDevice*)g_ptr_array_index(devices, i);
        NMDeviceType type = nm_device_get_device_type(device);
        if (type != NM_DEVICE_TYPE_WIFI_P2P)
            continue;

        AC_DEBUG("Setting interface: %s", nm_device_get_iface(device));
        OnInterfaceDeviceFound(device);
        break;
    }

    session_available_ = true;
    ConfigureFromCapabilities();

    TriggerReadyChange();

    StartScan();
}

void NetworkManager::TriggerReadyChange()
{
    if (delegate_)
        delegate_->OnReadyChanged();
}

void NetworkManager::OnServiceFound(GDBusConnection *connection, const gchar *name, const gchar *name_owner, gpointer user_data) {
    boost::ignore_unused_variable_warning(connection);
    boost::ignore_unused_variable_warning(name);
    boost::ignore_unused_variable_warning(name_owner);

    auto inst = static_cast<ac::WeakKeepAlive<NetworkManager>*>(user_data)->GetInstance().lock();

    if (not inst)
        return;

    inst->Initialize(true);
}

void NetworkManager::OnServiceLost(GDBusConnection *connection, const gchar *name, gpointer user_data) {
    boost::ignore_unused_variable_warning(connection);
    boost::ignore_unused_variable_warning(name);

    auto inst = static_cast<ac::WeakKeepAlive<NetworkManager>*>(user_data)->GetInstance().lock();

    if (not inst)
        return;

    inst->ReleaseInternal();
}

static void peer_added_cb(NMDeviceWifiP2P* device, NMWifiP2PPeer* peer, NetworkManager* nm);

static void on_peer_wfd_ie_notify_cb(NetworkManager *nm, GParamSpec *pspec, NMWifiP2PPeer *peer)
{
    AC_DEBUG("WFDIEs for ignored peer \"%s\" (%s) changed, trying to re-add",
             nm_wifi_p2p_peer_get_name (peer),
             nm_wifi_p2p_peer_get_hw_address (peer));

    g_signal_handlers_disconnect_by_func(peer, (gpointer)on_peer_wfd_ie_notify_cb, nm);
    peer_added_cb(NM_DEVICE_WIFI_P2P(nm->NmDevice()), peer, nm);
}

static void peer_added_cb(NMDeviceWifiP2P* device, NMWifiP2PPeer* peer, NetworkManager* nm)
{
    GBytes *wfd_ies = nm_wifi_p2p_peer_get_wfd_ies(peer);

    /* Assume this is not a WFD Peer if there are no WFDIEs set. */
    if (!wfd_ies || g_bytes_get_size (wfd_ies) == 0)
    {
        AC_DEBUG("Ignoring peer \"%s\" (%s) for now as it has no WFDIEs set",
                 nm_wifi_p2p_peer_get_name (peer),
                 nm_wifi_p2p_peer_get_hw_address (peer));

        g_signal_connect(peer, "notify::" NM_WIFI_P2P_PEER_WFD_IES,
                         (GCallback)on_peer_wfd_ie_notify_cb,
                         (gpointer)nm);
        return;
    }

    AC_DEBUG ("Found a new sink with peer \"%s\" (%s)",
              nm_wifi_p2p_peer_get_name (peer),
              nm_wifi_p2p_peer_get_hw_address (peer));

    nm->OnDeviceFound(peer);
}

static void peer_removed_cb(NMDeviceWifiP2P* device, NMWifiP2PPeer* peer, NetworkManager* nm)
{
    AC_DEBUG("Peer removed");
    /* Otherwise we may see properties changing to NULL before the object is destroyed. */
    g_signal_handlers_disconnect_by_func(peer, (gpointer)on_peer_wfd_ie_notify_cb, nm);

    if (!nm->Running())
        nm->OnDeviceLost(peer);
}

static void device_state_changed_cb(NMDevice *device, NMDeviceState new_state, NMDeviceState old_state, NMDeviceStateReason reason, NetworkManager* nm)
{
    AC_DEBUG ("Device state changed. It is now %i. Reason: %i", new_state, reason);

#if 0
    if (new_state > NM_DEVICE_STATE_UNAVAILABLE && new_state < NM_DEVICE_STATE_ACTIVATED ||
            new_state == NM_DEVICE_STATE_DEACTIVATING || new_state == NM_DEVICE_STATE_FAILED) {
        nm->StartScan();
    }
#endif

    if (new_state == NM_DEVICE_STATE_FAILED) {
        nm->Cancel();
        nm->AdvanceDeviceState(ac::kFailure);
    } else if (new_state == NM_DEVICE_STATE_ACTIVATED) {
        nm->AdvanceDeviceState(ac::kConnected);
        nm->TriggerReadyChange();
    } else if (new_state == NM_DEVICE_STATE_DISCONNECTED) {
        nm->Cancel();
        nm->AdvanceDeviceState(ac::kDisconnected);
    }
}

void NetworkManager::OnDeviceFound(NMWifiP2PPeer* peer)
{
    std::lock_guard<decltype(devices_mutex_)> lock(devices_mutex_);

    NetworkManagerDevice::Ptr acDevice = NetworkManagerDevice::Create(nm_device_, peer);
    devices_.push_back(acDevice);
    if (delegate_)
        delegate_->OnDeviceFound(acDevice);
}

void NetworkManager::OnDeviceLost(NMWifiP2PPeer* peer)
{
    std::lock_guard<decltype(devices_mutex_)> lock(devices_mutex_);

    int counter = 0;
    for (auto iter = devices_.rbegin(); iter != devices_.rend(); iter++) {
        if (delegate_ && (*iter)->NmPeer() == peer) {
            delegate_->OnDeviceLost(*iter);
            devices_.erase(devices_.begin() + counter);
            break;
        }
        counter++;
    }
}

void NetworkManager::OnInterfaceDeviceFound(NMDevice* device)
{
    if (nm_device_ != device) {
        nm_device_ = device;

        g_signal_connect(nm_device_,
                         "peer-added",
                         (GCallback)peer_added_cb,
                         (gpointer)this);

        g_signal_connect(nm_device_,
                         "peer-removed",
                         (GCallback)peer_removed_cb,
                         (gpointer)this);

        g_signal_connect(nm_device_,
                         "state-changed",
                         (GCallback)device_state_changed_cb,
                         (gpointer)this);
    }

    ReloadDevices();
}

void NetworkManager::OnInterfaceDeviceLost(NMDevice* device)
{
    if (device == nm_device_)
        nm_device_ = nullptr;
}

void NetworkManager::Initialize(bool firmware_loading) {
    if (firmware_loading && ac::Utils::GetEnvValue("AETHERCAST_NEED_FIRMWARE") == "1") {
        auto interface_name = ac::Utils::GetEnvValue("AETHERCAST_DEDICATED_P2P_INTERFACE");
        if (interface_name.empty())
            interface_name = "p2p0";

        AC_DEBUG("Firmware loading for interface '%s' requested", interface_name);

        firmware_loader_.SetInterfaceName(interface_name);
        AC_DEBUG("Loading WiFi firmware for interface %s", interface_name);
        firmware_loader_.TryLoad();
        return;
    }

    auto sp = shared_from_this();

    hostname_service_ = Hostname1Stub::Create(sp);

    manager_ = ManagerStub::Create();
    manager_->SetDelegate(sp);

    AC_DEBUG("Successfully initialized");
    sp->Start();
}

void NetworkManager::Release() {
    AC_DEBUG("");

    std::lock_guard<decltype(devices_mutex_)> lock(devices_mutex_);

    for (auto &iter : devices_) {
        if (delegate_)
            delegate_->OnDeviceLost(iter);
    }

    devices_.clear();

    ReleaseInternal();
}

void NetworkManager::ReleaseInternal() {
    ReleaseInterface();

    hostname_service_.reset();
    manager_.reset();
}

void NetworkManager::ReleaseInterface() {
    AC_DEBUG("");

    AdvanceDeviceState(ac::kDisconnected);

    driver_cmd_iface_.clear();
}

void NetworkManager::SetDelegate(ac::NetworkManager::Delegate *delegate) {
    delegate_ = delegate;
}

bool NetworkManager::Setup() {
    if (!Ready())
        return false;

    g_bus_watch_name_on_connection(connection_.get(),
                                   kBusName,
                                   G_BUS_NAME_WATCHER_FLAGS_NONE,
                                   &NetworkManager::OnServiceFound,
                                   &NetworkManager::OnServiceLost,
                                   new ac::WeakKeepAlive<NetworkManager>(shared_from_this()),
                                   nullptr);

    return true;
}

void NetworkManager::OnURfkillAvailable(GDBusConnection*, const gchar*, const gchar*, gpointer user_data) {
    auto inst = static_cast<ac::WeakKeepAlive<NetworkManager>*>(user_data)->GetInstance().lock();
    if (!inst)
        return;

    AC_DEBUG("URfkill is available");

    inst->rfkill_manager_ = URfkillManager::Create();
    inst->FinishRfkillInitialization();
}

void NetworkManager::OnURfkillNotAvailable(GDBusConnection*, const gchar*, gpointer user_data) {
    auto inst = static_cast<ac::WeakKeepAlive<NetworkManager>*>(user_data)->GetInstance().lock();
    if (!inst)
        return;

    AC_DEBUG("URfkill is not available, falling back to kernel rfkill manager");

    inst->rfkill_manager_ = KernelRfkillManager::Create();
    inst->FinishRfkillInitialization();
}

void NetworkManager::FinishRfkillInitialization() {
    g_source_remove(urfkill_watch_);
    urfkill_watch_ = 0;

    rfkill_manager_->SetDelegate(shared_from_this());
}

void NetworkManager::Scan(const std::chrono::seconds& timeout) {
    scan_timeout_ = g_timeout_add_seconds(timeout.count(), [](gpointer user_data) {
            auto inst = static_cast<ac::SharedKeepAlive<NetworkManager>*>(user_data)->ShouldDie();
            if (!inst)
            return FALSE;

            if (inst->Running())
            return FALSE;

            AC_WARNING("Reached a timeout while scanning");

            inst->ReloadDevices();
            inst->scan_timeout_ = 0;

            // We don't have an active group if we're not in connected or configuration
            // state so we don't have to care about terminating any group at this point.
            return FALSE;
}, new ac::SharedKeepAlive<NetworkManager>{shared_from_this()});

    Start();
    StartScan();
}

void NetworkManager::Start()
{
    if (nm_client) {
        AchieveDeviceAccess(nm_client);
        return;
    }

    NMClient* nmclient = nm_client_new(nullptr, nullptr);
    if (!nmclient) {
        AC_WARNING("Failed to create NetworkManager client");
        return;
    }
    AchieveDeviceAccess(nmclient);
}

void NetworkManager::CancelScan()
{
    AC_DEBUG("");
    if (nm_device_ && scan_cancellable) {
        nm_device_wifi_p2p_stop_find(NM_DEVICE_WIFI_P2P(nm_device_),
                                     scan_cancellable,
                                     nullptr,
                                     nullptr);
    }

    if (scan_timeout_ != 0) {
        g_source_remove(scan_timeout_);
        scan_timeout_ = 0;
    }

    //if (delegate_)
    //    delegate_->OnReadyChanged();
}

static void log_start_find_error(GObject *source, GAsyncResult *res, gpointer user_data)
{
    g_autoptr(GError) error = NULL;
    NMDeviceWifiP2P *p2p_dev = NM_DEVICE_WIFI_P2P(source);

    if (!nm_device_wifi_p2p_start_find_finish(p2p_dev, res, &error)) {
        AC_WARNING("Could not start P2P discovery: %s", error->message);
    } else {
        AC_DEBUG("Started P2P discovery");
        NetworkManager* nm = static_cast<NetworkManager*>(user_data);
        if (!nm) {
            AC_WARNING("Failed to cast NetworkManager");
            return;
        }
        nm->ReloadDevices();
    }
}

void NetworkManager::ReloadDevices()
{
    {
        std::lock_guard<decltype(devices_mutex_)> lock(devices_mutex_);

        for (auto &iter : devices_) {
            if (delegate_)
                delegate_->OnDeviceLost(iter);
        }
        devices_.clear();
    }

    const GPtrArray *peers = nm_device_wifi_p2p_get_peers(NM_DEVICE_WIFI_P2P(nm_device_));
    for (gint i = 0; i < peers->len; i++)
        peer_added_cb(NM_DEVICE_WIFI_P2P(nm_device_), (NMWifiP2PPeer*)g_ptr_array_index(peers, i), this);
}

void NetworkManager::StartScan()
{
    AC_DEBUG("");

    if (scan_cancellable) {
        return;
    }

    if (active_connection_) {
        return;
    }

    if (!nm_device_) {
        AC_WARNING("No NM Device set yet...");
        return;
    }

    scan_cancellable = g_cancellable_new();
    nm_device_wifi_p2p_start_find(NM_DEVICE_WIFI_P2P(nm_device_),
                                  nullptr,
                                  nullptr,
                                  log_start_find_error,
                                  this);

    if (delegate_)
        delegate_->OnReadyChanged();
}

void NetworkManager::Cancel() {
    if (connect_timeout_ != 0) {
        g_source_remove(connect_timeout_);
        connect_timeout_ = 0;
    }

    if (cancellable) {
        g_cancellable_cancel(cancellable);
        g_clear_object(&cancellable);
    }

    active_connection_ = nullptr;

    g_clear_object(&nm_client);
    g_clear_object(&nm_device_);
}

void NetworkManager::StartConnectTimeout() {
    AC_DEBUG("");

    connect_timeout_ = g_timeout_add_seconds(kConnectTimeout.count(), [](gpointer user_data) {
            auto inst = static_cast<ac::SharedKeepAlive<NetworkManager>*>(user_data)->ShouldDie();
            if (!inst)
            return FALSE;

            AC_WARNING("Reached a timeout while trying to connect with remote %s");

            inst->connect_timeout_ = 0;
            inst->AdvanceDeviceState(ac::kFailure);

            // We don't have an active group if we're not in connected or configuration
            // state so we don't have to care about terminating any group at this point.

            return FALSE;
}, new ac::SharedKeepAlive<NetworkManager>{shared_from_this()});
}

void NetworkManager::StopConnectTimeout() {
    if (connect_timeout_ == 0)
        return;

    AC_DEBUG("");

    g_source_remove(connect_timeout_);
    connect_timeout_ = 0;
}

void NetworkManager::OnP2PConnected(GObject* source_object, GAsyncResult* res, gpointer user_data)
{
    g_autoptr(GError) error = NULL;
    AC_DEBUG("Got P2P connection");

    auto inst = static_cast<ac::WeakKeepAlive<NetworkManager>*>(user_data)->GetInstance().lock();
    if (!inst)
        return;

    StopConnectTimeout();

    active_connection_ = nm_client_add_and_activate_connection2_finish(NM_CLIENT(source_object), res, NULL, &error);
    if (!active_connection_) {
        current_device_.reset();

        // Operation was aborted
        if (g_error_matches(error, G_IO_ERROR, G_IO_ERROR_CANCELLED))
            return;

        AC_WARNING("Error activating connection: %s", error->message);
        AdvanceDeviceState(ac::kFailure);
        return;
    }

    AdvanceDeviceState(ac::kAssociation);

#if 0
    AdvanceDeviceState(ac::kConnected);
    // Affirm CancelScan running after the state has been set to connected,
    // as CancelScan() will trigger the OnReadyChanged() signal.
    CancelScan();
#endif
}

static void P2PConnected(GObject* source_object, GAsyncResult* res, gpointer user_data)
{
    auto inst = static_cast<NetworkManager*>(user_data);
    if (!inst)
        return;

    inst->OnP2PConnected(source_object, res, user_data);
}

bool NetworkManager::Connect(const ac::NetworkDevice::Ptr& device) {
    NetworkManagerDevice* nmddevice = (NetworkManagerDevice*)device.get();
    if (!nmddevice) {
        AC_WARNING("Failed to get NetworkManagerDevice from NetworkDevice");
        return false;
    }

    current_device_ = device;

    AC_DEBUG("address %s", device->Address());

    NMWifiP2PPeer* nm_peer = nmddevice->NmPeer();

    AdvanceDeviceState(ac::kAssociation);

    g_autoptr(NMConnection) connection = NULL;
    g_autoptr(GVariantBuilder) builder = NULL;
    g_autoptr(GBytes) wfd_ies = NULL;
    GVariant* options = NULL;
    NMSetting* general_setting;
    NMSetting* p2p_setting;
    NMSetting* ipv4_setting;
    NMSetting* ipv6_setting;

    builder = g_variant_builder_new(G_VARIANT_TYPE_VARDICT);
    g_variant_builder_add(builder, "{sv}", "bind-activation", g_variant_new_string("dbus-client"));
    g_variant_builder_add(builder, "{sv}", "persist", g_variant_new_string("volatile"));

    options = g_variant_builder_end(builder);

    wfd_ies = g_bytes_new_static(wfd_ies_->bytes, wfd_ies_->length);

    connection = nm_simple_connection_new();

    general_setting = nm_setting_connection_new();
    nm_connection_add_setting(connection, general_setting);

    p2p_setting = nm_setting_wifi_p2p_new();
    nm_connection_add_setting(connection, p2p_setting);
    g_object_set(p2p_setting, NM_SETTING_WIFI_P2P_WFD_IES, wfd_ies, NULL);

    // We never want to route on IPv4
    ipv4_setting = nm_setting_ip4_config_new();
    nm_connection_add_setting(connection, ipv4_setting);
    g_object_set(ipv4_setting,
                 NM_SETTING_IP_CONFIG_METHOD, NM_SETTING_IP4_CONFIG_METHOD_AUTO,
                 NM_SETTING_IP_CONFIG_NEVER_DEFAULT, TRUE,
                 NULL);

    // We do not need IPv6
    ipv6_setting = nm_setting_ip4_config_new();
    nm_connection_add_setting(connection, ipv6_setting);
    g_object_set(ipv6_setting,
                 NM_SETTING_IP_CONFIG_METHOD, NM_SETTING_IP6_CONFIG_METHOD_AUTO,
                 NM_SETTING_IP_CONFIG_NEVER_DEFAULT, TRUE,
                 NM_SETTING_IP_CONFIG_MAY_FAIL, TRUE,
                 NULL);

    // Activate connection
    StartConnectTimeout();

    cancellable = g_cancellable_new();
    nm_client_add_and_activate_connection2(nm_client,
                                           connection,
                                           nm_device_,
                                           nm_object_get_path(NM_OBJECT(nm_peer)),
                                           options,
                                           cancellable,
                                           P2PConnected,
                                           this);

    return true;
}

std::string NetworkManager::SelectHostname() {
    auto hostname = hostname_service_->PrettyHostname();
    if (hostname.length() == 0)
        hostname = hostname_service_->StaticHostname();
    if (hostname.length() == 0)
        hostname = hostname_service_->Hostname();
    if (hostname.length() == 0) {
        // Our last resort is to get the hostname via a system
        // call and not from the hostname service.
        char name[HOST_NAME_MAX + 1] = {};
        ::gethostname(name, HOST_NAME_MAX);
        hostname = name;
    }
    return hostname;
}

std::string NetworkManager::SelectDeviceType() {
    std::string oui = "0050F204";
    std::string category = "0001";
    std::string sub_category = "0000";

    auto chassis = hostname_service_->Chassis();
    if (chassis == "handset") {
        category = "000A";
        sub_category = "0005";
    }
    else if (chassis == "vm" || chassis == "container")
        sub_category = "0001";
    else if (chassis == "server")
        sub_category = "0002";
    else if (chassis == "laptop")
        sub_category = "0005";
    else if (chassis == "desktop")
        sub_category = "0006";
    else if (chassis == "tablet")
        sub_category = "0009";
    else if (chassis == "watch")
        sub_category = "00FF";

    return ac::Utils::Sprintf("%s%s%s", category, oui, sub_category);
}

bool NetworkManager::Disconnect(const ac::NetworkDevice::Ptr& device) {
    Cancel();
    StartScan();

    return true;
}

std::vector<ac::NetworkDevice::Ptr> NetworkManager::Devices() const {
    std::vector<ac::NetworkDevice::Ptr> values;

    std::lock_guard<decltype(devices_mutex_)> lock(devices_mutex_);

    std::transform(devices_.begin(), devices_.end(),
                   std::back_inserter(values),
                   [=](w11tng::NetworkManagerDevice::Ptr value) {
        return value;
    });

    return values;
}

ac::IpV4Address NetworkManager::LocalAddress() const {
    ac::IpV4Address address;

    AC_DEBUG("address %s", address);

    return address;
}

bool NetworkManager::Running() const {
    return active_connection_;
}

bool NetworkManager::Scanning() const {
    return scan_cancellable;
}

bool NetworkManager::Ready() const {
    if (!rfkill_manager_)
        return false;

    return !rfkill_manager_->IsBlocked(RfkillManager::Type::kWLAN);
}

void NetworkManager::AdvanceDeviceState(ac::NetworkDeviceState state) {
    if (state == ac::kDisconnected) {
        if (ac::NetworkUtils::SendDriverPrivateCommand(driver_cmd_iface_,
                                                       BuildMiracastModeCommand(MiracastMode::kOff)) < 0)
        {
            AC_DEBUG("Failed to disable WiFi driver miracast mode (not supported?)");
        } else {
            AC_DEBUG("Disabled WiFi driver miracast mode");
        }
    }

    {
        NetworkManagerDevice* nmddevice = (NetworkManagerDevice*)current_device_.get();
        if (nmddevice) {
            nmddevice->SetState(state);
        } else {
            AC_WARNING("Failed to get NetworkManagerDevice");
        }
    }

#if 0
    if (state == ac::kConnected || state == ac::kDisconnected) {
        session_available_ = (state != ac::kConnected);
        ConfigureFromCapabilities();
    }
#endif

    if (delegate_ && current_device_)
        delegate_->OnDeviceStateChanged(current_device_);
}

void NetworkManager::HandleConnectFailed() {
    AdvanceDeviceState(ac::kFailure);
    StopConnectTimeout();
}

void NetworkManager::OnFirmwareLoaded() {
    // Pass through when firmware was successfully loaded and
    // do all other needed initialization stuff
    Initialize();
}

void NetworkManager::OnFirmwareUnloaded() {
}

void NetworkManager::SetCapabilities(const std::vector<Capability> &capabilities) {
    if (capabilities == capabilities_)
        return;

    capabilities_ = capabilities;
    ConfigureFromCapabilities();
}

std::vector<NetworkManager::Capability> NetworkManager::Capabilities() const {
    return capabilities_;
}

DeviceType NetworkManager::GenerateWfdDeviceType() {
    DeviceType device_type = DeviceType::kSource;
    bool has_source = false, has_sink = false;

    for (auto capability : capabilities_) {
        if (capability == Capability::kSource)
            has_source = true;
        else if (capability == Capability::kSink)
            has_sink = true;
    }

    if (has_sink && !has_source)
        device_type = DeviceType::kPrimarySink;
    else if (!has_sink && has_source)
        device_type = DeviceType::kSource;
    else if (has_sink && has_source)
        device_type = DeviceType::kDualRole;

    return device_type;
}

void NetworkManager::ConfigureFromCapabilities() {
    if (!manager_)
        return;

    InformationElement ie;
    auto sub_element = new_subelement(kDeviceInformation);
    auto dev_info = (DeviceInformationSubelement*) sub_element;

    auto device_type = GenerateWfdDeviceType();

    AC_DEBUG("device type %d session availability %d",
             device_type,
             session_available_);

    dev_info->session_management_control_port = htons(7236);
    dev_info->maximum_throughput = htons(50);
    dev_info->field1.device_type = device_type;
    dev_info->field1.session_availability = session_available_;
    ie.add_subelement(sub_element);

    wfd_ies_ = ie.serialize();
}

void NetworkManager::OnManagerReady() {
    // TODO
}

void NetworkManager::OnManagementInterfaceReady() {
    driver_cmd_iface_ = ac::Utils::GetEnvValue("AETHERCAST_DRIVER_CMD_IFACE", "");
}

std::string NetworkManager::BuildMiracastModeCommand(MiracastMode mode) {
    return ac::Utils::Sprintf("MIRACAST %d", static_cast<int>(mode));
}

void NetworkManager::OnHostnameChanged() {
    AC_DEBUG("");
    // TODO
}

void NetworkManager::OnRfkillChanged(const RfkillManager::Type &type) {
    if (type != RfkillManager::Type::kWLAN)
        return;

    if (delegate_)
        delegate_->OnReadyChanged();
}

} // namespace w11tng
